﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class fMain : Form
    {
        public fMain()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void tbY_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbX1.Text) || (string.IsNullOrEmpty(tbX2.Text))) {

                tbY.Text = " Не введено даних!";
            }

            double x1 = double.Parse(tbX1.Text);
            double x2 = double.Parse(tbX2.Text);

            double y = Math.Pow(Math.Cos(Math.Exp(x1 + 2 * x2 + 9 / 0.666)),3);

            if (x1 > x2)
            {
                tbY.Text = x1.ToString("0.###");
            }

            else
            {
                tbY.Text = x2.ToString("0.###");
            }

            tbY.Text = y.ToString("0.###");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tbX1.Text = string.Empty;
            tbX2.Text = string.Empty;
            tbY.Text = string.Empty;

        }

        private void button3_Click(object sender, EventArgs e)
        {

            Application.Exit();
        }

        private void tbX2_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbX1_TextChanged(object sender, EventArgs e)
        {

        }

        private void fMain_Load(object sender, EventArgs e)
        {

        }

        private void bigNumber_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbX1.Text) || (string.IsNullOrEmpty(tbX2.Text)))
            {

                tbY.Text = " Не введено даних!";
            }

            double x1 = double.Parse(tbX1.Text);
            double x2 = double.Parse(tbX2.Text);

            if (x1 > x2)
            {
                tbY.Text = x1.ToString("0.###");
            }

            else
            {
                tbY.Text = x2.ToString("0.###");
            }
        }
    }
}
